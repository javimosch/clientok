(function() {
    function _select(selector, elem) {
        var arr = [].slice.call(elem && elem.querySelectorAll(selector) || document.querySelectorAll(selector), 0);
        arr._remove = function() {
            arr.forEach(function(o) {
                o.parentNode.removeChild(o);
            });
        }
        return arr;
    };

    function _ctrlContext() {
        var self = this;
        self.$class = {
            has: function(ele, cls) {
                return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
            },
            add: function(ele, cls) {
                if (!this.has(ele, cls)) ele.className += " " + cls;
            },
            remove: function(ele, cls) {
                if (this.has(ele, cls)) {
                    var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                    ele.className = ele.className.replace(reg, ' ');
                }
            }
        };
        var $gen = self.$gen = {
            guid: function(prefix) {
                return (prefix || 'id') + (new Date()).getTime();
            }
        };

        function ajaxSetContent(elem, url, cb) {
            ajax(url).then(function(jsonObj, txt, xhr) {
                elem.innerHTML = txt;
                cb && cb(txt, elem);
            });
        }

        function _bindsCompileExecuteActions(arr, elem, ctx) {
            for (var index in arr) {
                (function(ctx, item, elem) {
                    ctx[item[0]] && ctx[item[0]].apply(elem, item[1]);
                    !ctx[item[0]] && console.warn('%s: Function expected: %s', elem.id, item[0]);
                    ctx[item[0]] && console.info('%s: Action executed: %s', elem.id, item[0]);
                })(ctx, arr[index], elem);
            }
        }

        function _bindsCompileScopeBinds(arr, elem, ctx) {
            for (var index in arr) {
                (function(ctx, item, elem) {
                    var args = item
                    if (args.attr && args.to) {
                        self.$binds.manual(args.to, $el(elem), args.attr, ctx).watch(function(v) {});
                    }
                    if (args.self) {
                        ctx[args.self] = $el(elem);
                    }
                })(ctx, arr[index], elem);
            }
        }
        var $http = self.$http = {
            root: '/git/namtok/namtok/',
            req: function(controller, params, cb) {
                //console.log(this.root + controller);
                ajax(this.root + controller).then(function(obj, txt, xhr) {
                    cb && cb(obj, txt, xhr);
                }).error(function(xhr) {
                    console.warn('api.call: error ' + xhr);
                });
            }
        };
        self.$binds = {
            init: function(context) {
                context.$on('update.binds', function(ctx) {
                    self.$binds.update(ctx);
                });
                self.$binds.init = undefined;
            },
            update: function(context) {
                this.init && this.init(context);
                this._compileInclude(context);
                //this._compileEvt(context);
                this._compileEvtKd(context);
                this._compileBinds(context);
            },
            _compileBinds: function(ctx) {
                var type = 'binds';
                _select('[data-' + type + ']').forEach(function(o) {
                    if (o.dataset.clone && o.dataset.clone === '0') return;
                    $el(o).
                    _do(function(o) {
                        o.id = o.id || $gen.guid();
                        var args = null;
                        eval('args=' + o.dataset[type] + ';');
                        //init
                        args.init && _bindsCompileExecuteActions(args.init, o, ctx);
                        //scope
                        args.scope && _bindsCompileScopeBinds(args.scope, o, ctx);
                        //evts
                        for (var evtType in args.evts) {
                            evtTypeArr = args.evts[evtType];
                            for (var index in evtTypeArr) {
                                (function(ctx, item, elem) {
                                    //console.log(item,evtType,elem);
                                    self.$evt.on(elem, evtType, function() {
                                        ctx[item[0]] && ctx[item[0]].apply(elem, item[1]);
                                        !ctx[item[0]] && console.warn('%s: Function expected: %s', elem.id, item[0]);
                                    });
                                })(ctx, evtTypeArr[index], o);
                            }
                        }
                        delete o.dataset[type];
                    });
                });
            },
            _compileInclude: function(context) {
                var cb = function(txt, elem) {
                    if (elem._includeArgs.unique) {
                        var p = elem.parentNode
                        $el(p)._select("[data-include-item]").forEach(function(c) {
                            //console.log('%s == %s',c.dataset.includeItem,elem._includeReference.id);
                            if (c.dataset.includeItem != elem._includeReference.id) {
                                self.$class.add(c, 'hidden');
                            }
                        });
                    }
                    self.$binds.update(context);
                };
                _select('[data-include]').forEach(function(o) {
                    $el(o).
                    _do(function(o) {
                        o.id = o.id || $gen.guid();
                        var args = null;
                        eval('args=' + o.dataset.include + ';');
                        o._includeArgs = args;
                        if (args.on !== undefined) {
                            o.dataset.info = 'esperando evento ' + args.on;
                            var includedItem = document.createElement('div');
                            includedItem._includeArgs = args;
                            includedItem.dataset.includeItem = o.id;
                            includedItem._includeReference = o;
                            o.parentNode.appendChild(includedItem);
                            context.$on(args.on, function() {
                                if (includedItem.innerHTML !== '') {
                                    self.$class.remove(includedItem, 'hidden');
                                }
                                ajaxSetContent(includedItem, args.url, cb)
                                self.$binds.update(context);
                            });
                        } else {
                            ajaxSetContent(o, args.url, cb)
                        }
                        delete o.dataset.include;
                    });
                });
            },
            manual: function(evalProp, selector, elemAttr, context) {
                var elem = typeof selector === 'string' && _select(selector)[0] || selector;
                var uuid = evalProp + '.' + (elem.id || new Date().getTime()) + '.' + elemAttr;
                var actions = {
                    watch: function(cb) {
                        self.$on(uuid + '.update', cb);
                    }
                }
                self.$binds.__binds = self.$binds.__binds || {};
                var _binds = self.$binds.__binds;
                _binds[uuid] = _binds[uuid] || {
                    propValue: getPropVal(evalProp),
                    elemValue: getPropVal(evalProp),
                    regValue: getPropVal(evalProp)
                };
                var o = _binds[uuid];
                var fn = function() {
                    o.elemValue = (elem[elemAttr] !== undefined) ? elem[elemAttr] : elem.getAttribute(elemAttr);
                    o.propValue = getPropVal(evalProp);
                    if (o.propValue !== o.regValue) {
                        updateElemValue(elem, elemAttr, o.propValue);
                        o.regValue = o.propValue;
                        self.$emit(uuid + '.update', o.regValue, o);
                    } else {
                        if (o.elemValue !== o.regValue) {
                            updatePropValue(o.elemValue);
                            o.regValue = o.elemValue;
                            self.$emit(uuid + '.update', o.regValue, o);
                        }
                    }
                    //console.log(o);
                    next();
                };

                function getPropVal(evalProp) {
                    return context[evalProp] || "";
                }

                function updatePropValue(val) {
                    o.propValue = val;
                    context[evalProp] = val;
                }
                var updateElemValue = function(elem, elemAttr, val) {
                    if (elem[elemAttr] !== undefined) elem[elemAttr] = val;
                    else elem.setAttribute(elemAttr, val);
                    o.elemValue = val;
                };
                var next = function() {
                    if (o.elemValue === null) {
                        elem = typeof selector === 'string' && _select(selector)[0] || selector;
                    }
                    setTimeout(function() {
                        fn.apply(context);
                    }, 30);
                }
                updateElemValue(elem, elemAttr, o.propValue);
                updatePropValue(elem.value);
                next();
                return actions;
            },
            _compileEvtKd: function(context) {
                var selfLocal = this;
                _select('[data-evt-kd]').forEach(function(o) {
                    if (o.dataset.clone && o.dataset.clone === '0') return;
                    $el(o).
                    _do(function(o) {
                        var args = null;
                        eval('args=' + o.dataset.evtKd + ';'); //JSON.parse(o.dataset.evt);
                        (function() {
                            var _fn = function() {
                                if (args.focusRequired && o !== document.activeElement) return;
                                if (typeof context[args.cb] !== 'function') {
                                    console.warn(args.cb + ' function not found.');
                                    return;
                                }
                                context[args.cb].apply(o, [o]);
                            };
                            var key = args.key + '.' + args.action;
                            selfLocal._kd = selfLocal._kd || {};
                            selfLocal._kd[key] = selfLocal._kd[key] || [];
                            var arrFn = selfLocal._kd[key];
                            arrFn.push(_fn);
                            kd[args.key][args.action](function() {
                                arrFn.forEach(function(fn) {
                                    fn();
                                });
                            });
                        })();
                        delete o.dataset.evt;
                    });
                });
            }
        };
        self.$evt = {
            on: function(elem, event, fn) {
                function listenHandler(e) {
                    var ret = fn.apply(this, arguments);
                    if (ret === false) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    return (ret);
                }

                function attachHandler() {
                    var ret = fn.call(elem, window.event);
                    if (ret === false) {
                        window.event.returnValue = false;
                        window.event.cancelBubble = true;
                    }
                    return (ret);
                }
                if (elem.addEventListener) {
                    elem.addEventListener(event, listenHandler, false);
                    return listenHandler;
                } else {
                    elem.attachEvent("on" + event, attachHandler);
                    return attachHandler;
                }
            },
            off: function(e, evt, fn) {
                if (e.removeEventListener) {
                    e.removeEventListener(evt, fn);
                } else if (e.detachEvent) {
                    e.detachEvent('on' + evt, fn);
                }
            }
        };
        var ajax = self.ajax = function(url, params) {
            var _cb = null,
                ready = false,
                _err = null,
                hasErr = false;

            function getParsedResponse(xhr) {
                var parsed = null
                try {
                    parsed = JSON.parse(xhr.responseText);
                } catch (e) {
                    //
                }
                return parsed;
            }
            var xhr = new XMLHttpRequest();
            xhr.open(params && 'POST' || 'GET', encodeURI(url));
            xhr.onload = function() {
                if (xhr.status === 200) {
                    ready = true;
                    if (_cb) {
                        _cb && _cb(getParsedResponse(xhr), xhr.responseText, xhr);
                    }
                } else {
                    _err && _err(xhr);
                    hasErr = true;
                }
            };
            xhr.send(params);
            var rta = {
                then: function(cb) {
                    if (ready) {
                        cb && cb(getParsedResponse(xhr), xhr.responseText, xhr);
                    } else {
                        _cb = cb;
                    }
                    return rta;
                },
                error: function(err) {
                    if (hasErr) {
                        err && err(xhr);
                    } else {
                        _err = err;
                    }
                    return rta;
                }
            };
            return rta;
        };
        self.$select = _select;
        self.$anim = {
            attr: (function() {
                var stop = function() {};
                var randomPropName = '',
                    animating = false;
                return function(elem, attrName, val, seconds, force) {
                    if (animating && !(force || false)) {
                        console.warn('overlaping omitted!');
                        return
                    }
                    stop && stop(); //stop current animation
                    animating = true;
                    randomPropName = 'prop' + new Date().getTime();
                    $el(elem)._do(function(e) {
                        stop = function() {
                            clearTimeout(e._waitTimeout);
                            e._do("setAttribute('" + attrName + "','" + e[randomPropName] + "')");
                            delete e[randomPropName];
                            animating = false;
                            stop = null;
                        };
                        e._do("getAttribute('" + attrName + "')", randomPropName);
                        e._do("setAttribute('" + attrName + "','" + val + "')");
                        e._wait(seconds, function(e) {
                            stop && stop();
                        });
                    });
                }
            })()
        }
        var $el = null;
        $el = self.$el = function(selectorOrElement) {
            var e = null;
            if (typeof selectorOrElement === 'string') {
                e = document.querySelectorAll(selectorOrElement)[0];
            } else {
                if (typeof selectorOrElement === 'object' || typeof selectorOrElement === 'function') {
                    e = selectorOrElement;
                } else {
                    console.error('$el wrong argument: %s type %s', selectorOrElement, typeof selectorOrElement);
                }
            }
            if (e == null || e == undefined) {
                var fn = function() {
                    return this;
                };
                fn._set = fn;
                fn._do = fn;
                fn._wait = fn;
                fn._rta = fn;
                fn._then = fn;
                fn._error = function(cb) {
                    cb && cb(e, (selectorOrElement + ' returns dummy'));
                };
                //            console.warn(selectorOrElement+' returns dummy');
                return fn;
            }
            var self = e;
            var apply = function(e) {
                var _err = null;
                e._select = function(s) {
                    return _select(s, e);
                };
                e._then = function(cb) {
                    cb && cb(e);
                    return e;
                };
                e._error = function(cb) {
                    _err = cb;
                    return e;
                };
                e._set = function(p) {
                    for (var x in p) {
                        e.setAttribute(x, p[x]);
                    }
                    return e;
                }
                e._do = function(ev, propToSave) {
                    //try {
                        if (typeof ev === 'function') {
                            ev(e);
                        } else {
                            var r = eval("e." + ev);
                            if (r !== undefined) {
                                if (propToSave) {
                                    e[propToSave] = r;
                                } else {
                                    e._rta = r;
                                }
                            }
                            if (typeof r === 'object' || typeof r === 'function') {
                                apply(r);
                            }
                        }
                        return e;
                   // } catch (ex) {
                     //   if (_err) _err(ex, 'Exception rised in _do');
                       // else throw ex;
                    //}
                    return e;
                }
                e._wait = function(seconds, then) {
                    e._waitTimeout = setTimeout(function() {
                        then && then(e);
                    }, seconds * 1000);
                };
                return e;
            }
            e = apply(e);
            return e;
        };
    }

    function eventable(o) {
        var events = {};
        o.$on = function(n, fn) {
            events[n] = events[n] || [];
            var id = new Date().getTime();
            events[n].push({
                id: id,
                fn: fn
            });
            //console.info('evt-$on->'+n);
            return {
                name: n,
                id: id
            };
        };
        o.$length = function(n) {
            events[n] = events[n] || [];
            return events[n].length;
        };
        o.$emit = function(n, p, ctx) {
            events[n] = events[n] || [];
            for (var x in events[n]) {
                console.info(n + '{id:' + events[n][x].id + '}');
                events[n][x].fn.apply(ctx || events[n][x], [p]);
            }
            if (events[n].length === 0) {
                console.info(n + '(zero bindings)');
                return false;
            } else {
                return true;
            }
        };
        o.$one = function(n, p) {
            events[n] = events[n] || [];
            for (var x; x < events[n]; x++) {
                events[n][x].fn(p);
                delete events[n][x];
            }
        };
        o.$off = function(onrta) {
            events[onrta.name] = events[onrta.name] || [];
            for (var x; x < events[onrta.name]; x++) {
                if (events[onrta.name][x].id == onrta.id) {
                    delete events[onrta.name][x];
                }
            }
        };
        o.$info = function() {
            for (var x; x < events; x++) {
                var evt = events[x];
                console.info(x + ' (' + evt.length + ')');
            }
        }
        return o;
    }

    function keyAt(obj, index) {
        var c = 0;
        for (var x in obj) {
            if (c == index) return x;
        }
        return null;
    }

    function _compileController() {
        _select("[data-controller]").forEach(function(o) {
            var module = eventable({
                _name: o.dataset.controller
            });
            _ctrlContext.apply(module); //magic happens here
            _namtok.$emit('controller.' + module._name, null, module);
            _namtok.__states = _namtok.__states || {};
            _namtok.__states[module._name] = module;
            _namtok.state = function(n) {
                n = n || keyAt(_namtok.__states, 0);
                var mod = _namtok.__states[n];
                var props = {};
                for (var x in mod) {
                    if (typeof mod[x] !== 'function') props[x] = mod[x];
                }
                return props;
            };
            delete o.dataset.controller;
            o.dataset.controlledBy = name;
        });
    }
    setInterval(function() {
        _compileController(); //life start here
    }, 1000);
    var _namtok = eventable({
        eventable: eventable
    });
    this.$namtok = _namtok;
}).apply(window);